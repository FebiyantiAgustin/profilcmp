<!doctype html>
<html>
	<head>
		<title> Profil Company </title>
			<meta charset="UTF-8">
			<meta name="viewport" content="width=device-width, initial-scale=1">
			<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/css/style.css">
			<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
			<link href="//netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
			<script src="//netdna.bootstrapcdn.com/bootstrap/3.0.3/js/bootstrap.min.js"></script>
			<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
			<meta name="viewport" content="width=device-width, initial-scale=1.0">
			<!--Bootstrap 4-->
			<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
			<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">
			<!--icons-->
			<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" />
			<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
			<!--vendors-->
			<link rel="stylesheet" href="<?php echo base_url() ?>assets/js/vendors/daterangepicker/daterangepicker.css" />
			<!--custom css-->
			<link rel="stylesheet" href="<?php echo base_url() ?>assets/css/components.css" />
			<link rel="stylesheet" href="<?php echo base_url() ?>assets/css/colors.css" />
			<link rel="stylesheet" href="<?php echo base_url() ?>assets/css/demo.css" />
			

		<body>
		<!-- Navbar -->
				 <nav class="navbar navbar-toggleable-md fixed-top navbar-transparent sticky-navigation">
            <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                <span class="ion-grid"></span>
            </button>
            <div class="container">
                <a class="navbar-brand" href="#">
                    <img src="http://localhost/WebProfilCompany/assets/img/logo.PNG" class="nav-brand-logo" />
                </a>
                <div class="collapse navbar-collapse" id="navbarCollapse">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item">
                            <a class="nav-link page-scroll" href="#home">Home</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link page-scroll" href="#features">About</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link page-scroll" href="#team">Team</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link page-scroll" href="#contact">Contact</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
			<!-- Header -->
			 <section class="hero-header" id="home" style="background-image: url(http://localhost/WebProfilCompany/assets/img/jagalab.PNG)">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8 offset-md-2">
                            <div class="brand">
                                <h1 class="heading-light"></h1>
                                <p class="lead mb-5">
                                </p>
                                
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
			
			<!-- Content -->
			<section class="bg-alt" id="features">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8 offset-md-2">
                            <div class="title text-center">
                                <h2 class="profile">Profile</h2>
                                <p class="lead text-muted">
                                    Write a short but precise description of your product here. This paragraph is 
                                    helpful to explain your product in detail.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="row text-center mt-4">
                        <div class="col-sm-4">
                            <div class="card border-none">
                                <div class="card-block">
                                    <div class="mb-3">
                                    
                                    </div>
                                  
                                    <p class="text-muted">
                                        Explain each individual feature here in more detail - write at least 3-4
                                        sentences. Do not talk about irrelevant stuff.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="card border-none">
                                <div class="card-block">
                                    <div class="mb-3">
                                     
                                    </div>
                                  
                                    <p class="text-muted">
                                        Explain each individual feature here in more detail - write at least 3-4
                                        sentences. Do not talk about irrelevant stuff.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="card border-none">
                                <div class="card-block">
                                    <div class="mb-3">
                                    
                                    </div>
                                 
                                    <p class="text-muted">
                                        Explain each individual feature here in more detail - write at least 3-4
                                        sentences. Do not talk about irrelevant stuff.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
							
								
				<div class="container">
				  <div class="iconcontainer">
					<div class="row">
					  <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
						<div class="iconbox">
						  <div class="iconbox-icon">
							<span class="glyphicon glyphicon-book"></span>
						  </div>
						  <div class="featureinfo">
							<h4 class="text-center">Vision</h4>
							<p>
							  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corrupti atque, tenetur quam aspernatur corporis at explicabo nulla dolore necessitatibus doloremque exercitationem sequi dolorem architecto perferendis quas aperiam debitis dolor soluta!
							</p>
						  </div>
						</div>
					  </div>
						 <div id="demo" class="collapse">
							Lorem ipsum dolor sit amet, consectetur adipisicing elit,
							sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
							quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
						  </div>
					  <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
						<div class="iconbox">
						  <div class="iconbox-icon">
							<span class="glyphicon glyphicon-tasks"></span>
						  </div>
						  <div class="featureinfo">
							<h4 class="text-center">Mision</h4>
							<p>
							  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corrupti atque, tenetur quam aspernatur corporis at explicabo nulla dolore necessitatibus doloremque exercitationem sequi dolorem architecto perferendis quas aperiam debitis dolor soluta!
							</p>
						  </div>
						</div>
					  </div>
					  <div id="demo" class="collapse">
						Lorem ipsum dolor sit amet, consectetur adipisicing elit,
						sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
						quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
					  </div>	
					</div>
						</div>
					  </div>
						<section>
					<div class="content">
					 <h3>My Google Maps Demo</h3>
					<div id="map" style="width:100%; height:400px;"></div>
						<script>
						  function initMap() {
							var uluru = {lat: -7.798627, lng: 110.413594};
							var map = new google.maps.Map(document.getElementById('map'), {
							  zoom: 4,
							  center: uluru
							});
							var marker = new google.maps.Marker({
							  position: uluru,
							  map: map
							});
						  }
						</script>
			<script async defer
			src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAJsvEqMn-48mexoa5ROlH90lwAgtl9afE&callback=initMap">
			</script>
			</section>
			</div>
				<section class="bg-alt" id="contact">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8 offset-md-2">
                            <div class="title text-center">
                                <h2 class="heading-semi-bold text-primary">Contact Us</h2>
                                <p class="lead text-muted">
                                    W'd love to hear from you!
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="row text-center mt-4">
                        <div class="col-md-8 offset-md-2">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Full Name" />
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Your Email Adress" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-group mt-4">
                                <textarea class="form-control" placeholder="Message" rows="5"></textarea>
                            </div>
                            <div class="form-group mt-4">
                                <input type="submit" class="btn btn-primary btn-round" value="Send Message" />
                            </div>
                        </div>
                    </div>
                </div>
            </section>
				<!-- Footer -->
						 <section class="colored-section bg-inverse footer pt-5">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-6 offset-sm-3 text-center">
                            <ul class="nav justify-content-center">
           
                            <p class="mt-4 text-muted">
                                &copy; 2018 Jagalab. 
                                <a href="https://wireddots.com">Wired Dots</a>. Designed by <a href="https://twitter.com/attacomsian">@Jagalab.com</a>
                            </p>
                        </div>
                    </div>
                </div>
            </section>
				<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
				<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"></script>
				<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js"></script>
				<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
				<script src="https://cdnjs.cloudflare.com/ajax/libs/wow/1.1.2/wow.js"></script>
				<!--vendors-->
				<script src="<?php echo base_url() ?>assets/js/vendors/moment.min.js"></script>
				<script src="<?php echo base_url() ?>assets/js/vendors/daterangepicker/daterangepicker.js"></script>
				<!--custom js-->
				<script src="<?php echo base_url() ?>assets/js/app.js"></script>
	</body>	
</html>