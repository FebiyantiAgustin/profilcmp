<!DOCTYPE>
<html>
<head>
    <title>data</title>
    <link href="<?php echo base_url()?>css/style.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="http://localhost/profilcmp/template//jquery-1.4.js"></script>
	<link href="<?php echo base_url();?>template/css/tabel.css" rel="stylesheet">
<script>
        $().ready(function(){
            $("#tombolTambah").click(function(){
                $.ajax({
                    url : "<?php base_url() ?>admin/tambah",
                    beforeSend: function(){
                                        $("#data").html("Loading...");
                                    },
                    success:    function(html){
                                    $("#data").html(html);
                                    $("#btnSimpan").show();
                                    $("#tombolTambah").hide();
 
                                }                
                });            	
            });   
		
		$("#btnSimpan").click(function(){                	
                var nis = $("#no_profil").val();
                var nama = $("#judul_profil").val();
                var kelas = $("#isi_profil").val();
               
              
                
                $.ajax({
                    url : "<?php echo base_url() ?>index.php/admin/tambah",
                    type: "post",
                    beforeSend: function(){
                                        $("#data").html("Loading...");
										},
                    data    : "no_profil="+no_profil+"&judul_profil="+judul_profil+"isi_profil="+isi_profil,
                    success:    function(html){
                                $("#tombolTambah").show();                                 
                                 $("#btnSimpan").hide();                                 
                                    $("#data").load("<?php echo base_url() ?>index.php/admin/index/ #data");
                                    $("#notifikasi").html('Data berhasil disimpan');                                    
                                    $("#notifikasi").fadeIn(2500);
                                    $("#notifikasi").fadeOut(2500);                                
                                }                
                });            
            });  

		$("#tombolUpdate").click(function(){                    
                var nis = $("#no_profil").val();
                var nama = $("#judul_profil").val();
                var kelas = $("#isi_profil").val();
              
                                   
                $.ajax({
                    url : "<?php echo base_url() ?>index.php/admin/update",
                    type: "post",
                    beforeSend: function(){
                                        $("#data").html("Loading...");
                                        },
                    data    : "no_profil="+no_profil+"&judul_profil="+judul_profil+"&isi_profil="+isi_profil,
                    success:    function(html){
                                $("#tombolTambah").show();                                 
                                 $("#tombolUpdate").hide();                                 
                                    $("#data").load("<?php echo base_url() ?>index.php/admin/index #data");
                                    $("#notifikasi").html('Data berhasil diedit');                                    
                                    $("#notifikasi").fadeIn(2500);
                                    $("#notifikasi").fadeOut(2500);                
                                    }                
                    });            
                });			
			});
		
		
		function edit(no_profil){
			$().ready(function(){
				$.ajax({
					url : "<?php echo base_url() ?>index.php/admin/edit/"+no_profil,
					beforeSend: function(){
										$("#data").html("Loading...");
									},
					success:    function(html){
								$("#tombolUpdate").show();
								$("#tombolTambah").hide();                                                       
								$("#data").html(html);                 
                                }                
                });                    
            });        
        }
		
		function hapus(no_profil){
            if(confirm('Yakin Menghapus?')){
            $().ready(function(){                                        
                $.ajax({                    
                    url : "<?php echo base_url() ?>index.php/admin/delete/"+no_profil,        
                    beforeSend: function(){
                                        $("#data").html("Loading...");
                                    },                                                
                    success:    function(html){
                                $("#tombolpdate").hide();                                 
                                 $("#tombolTambah").show();                                 
                                    $("#data").load('<?php echo base_url() ?>index.php/admin/index/ #data');                                                                                                    
                                }                
                });                    
            });    
        }        
        }
		
    </script>
<head>
<body>
	 <div id="notifikasi" style="display:none"></div>
    <div id="wraper">
    
        <div id="header">
            <h2>CRUD</h2>
        </div>
        
        <div id="content">
            <div id="paneltombol">
                <input type="button" class="tomboltambah" id="btnSimpan" value="Simpan" style="display:none">
                <input type="button" class="tomboltambah" id="tombolTambah" name="tombolTambah" value="Tambah Data">
				<input type="button" class="tomboltambah" id="tombolUpdate" name="tombolUpdate" value="Update" style="display:none">
            </div>
            
            <div id="data">                
                <table border="0" width="100%" cellspacing="0" >
                <tr>
                    <th>NO</th>
                    <th>JUDUL</th>
                    <th>ISI</th> 
					 <th colspan="2">Aksi</th>
                </tr>
                <?php foreach($profil as $baris):?>
					<tr>
						<td><?php echo $baris['no_profil'] ?></td>
						<td><?php echo $baris['judul_profil'] ?></td>
						<td><?php echo $baris['isi_profil'] ?></td>
						<td width="30"><input type="button" class="tombol" value="Edit" onclick="edit(<?php echo $baris['no_profil'] ?>)"></td>
						<td width="30"><input type="button" class="tombol" value="Hapus" onclick="hapus(<?php echo $baris['no_profil'] ?>)"></td>
					</tr>
				<?php endforeach ?>
                </table>
            </div>       
        </div>    
    </div>
                                
</body>

</html>

