<?php 
class M_Data extends CI_Model{
    function getDataProfil(){
        return $this->db->get('profil')->result();
    }
	function insertDataProfil($data){
        $this->db->insert('profil',$data);    
    }
	function DataProfil($no_profil){
        return $this->db->get_where('profil',array('no_profil'=>$no_profil))->result();        
	}
 function updateProfil($data = array()){
        $this->db->where('no_profil',$data['no_profil'])->update('profil',$data);
    }
	function deleteProfil($no_profil) {
        $this->db->where('no_profil',$no_profil)->delete('profil');    
    }

}
?>