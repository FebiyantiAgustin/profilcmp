CKEDITOR.editorConfig = function( config ) {
 // Define changes to default configuration here. For example:
 // config.language = 'fr';
 // config.uiColor = '#AADC6E';
 config.filebrowserBrowserUrl='kcfinder/browse.php?type=files';
 config.filebrowserImageBrowseUrl='kcfinder/browse.php?type=images';
 config.filebrowserFlashBrowseUrl = 'kcfinder/browse.php?type=flash';
 config.filebrowserUploadUrl = 'kcfinder/upload.php?type=files';
 config.filebrowserImageUploadUrl = 'kcfinder/upload.php?type=images';
 config.filebrowserFlashUploadUrl = 'upload.php?type=flash';
};